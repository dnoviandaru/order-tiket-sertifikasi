<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTiketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_tikets', function (Blueprint $table) {
            $table->id();
            $table->string('kelas',50);
            $table->string('nama');
            $table->string('nik',16);
            $table->string('no_hp');
            $table->string('tgl_berangkat',10);
            $table->integer('penumpang');
            $table->integer('penumpang_lansia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_tikets');
    }
}
