@extends('base')
@section('title','Komponen')
@section('komponen','active bg-warning')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Komponen</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('component.index')}}">Komponen</a></li>
                        <li class="breadcrumb-item active">Tambah</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    @foreach($errors->all() as $message)
                    <p class="m-0">{{"$message"}}</p>
                    @endforeach
                </div>
            @endif
            <div class="card card-primary">
                <!-- form start -->
                <form action="{{route('component.store')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Kode Komponen<span class="text-danger">*</span></label>
                            <input type="text" name="kode_komponen" value="{{old('kode_komponen')}}" class="form-control" placeholder="...">
                        </div>
                        <div class="form-group">
                            <label>Nama Komponen<span class="text-danger">*</span></label>
                            <input type="text" name="nama_komponen" value="{{old('nama_komponen')}}" class="form-control" placeholder="...">
                        </div>
                        <div class="form-group">
                            <label>Value Komponen<span class="text-danger">*</span></label>
                            <input type="text" name="value_komponen" value="{{old('value_komponen')}}" class="form-control" placeholder="...">
                        </div>
                        <div class="form-group">
                            <label>Grup Komponen<span class="text-danger">*</span></label>
                            <input type="text" list="grup_komponen" class="form-control" name="grup_komponen" autocomplete="off" placeholder="...">
                            <datalist id="grup_komponen">
                                @foreach ($komponen as $data)
                                    <option>{{$data->code_group }}</option>
                                @endforeach
                            </datalist>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('js')
<!-- @include('component/validation') -->
@include('component/select2')
@endpush
