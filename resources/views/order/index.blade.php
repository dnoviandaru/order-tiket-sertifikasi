<!DOCTYPE html>
<html lang="id-ID">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Primary Meta Tags -->

    <meta name="description" content="Deskripsi sistem anda">
    <meta name="keywords" content="keyword, sistem, anda, silahkan, diubah" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Custom CSS -->
    @stack('css')
</head>
<body class="accent-warning">
    <div class="h-100 d-flex flex-column flex-md-row align-items-center p-2 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <div class="container">
            <a class="d-flex align-items-center" href="{{route('order.index')}}">
                <img src="https://agricia.faperta.ugm.ac.id/wp-content/uploads/sites/377/2018/06/logo-ugm-png.png" width="65" height="80" class="d-inline-block align-top mr-3" alt="">
                <h1 class="text-primary">ORDER TIKET BUS</h1>
            </a>
        </div>
    </div>
    <!-- <main role="main" class="flex-shrink-0"> -->
    <div class="container d-flex align-items-center justify-content-center">
        <div class="container-fluid">
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    @foreach($errors->all() as $message)
                    <p class="m-0">{{"$message"}}</p>
                    @endforeach
                </div>
            @endif
            <div class="card card-primary w-100">
                <div class="card-header">
                    <h3 class="card-title">FORM PEMESANAN</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{route('order.store')}}" method="post">
                @csrf
                <form class="form-horizontal">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                            <div class="col-sm-10">
                            <input type="text" name="nama" value="{{old('nama')}}" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">NIK</label>
                            <div class="col-sm-10">
                            <input type="text" name="nik" value="{{old('nik')}}" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No HP</label>
                            <div class="col-sm-10">
                            <input type="text" name="no_hp" value="{{old('no_hp')}}" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kelas Penumpang</label>
                            <div class="col-sm-10">
                            <select name="kelas_penumpang" id="kelas" class="form-control">
                                <option value=""></option>
                                @foreach($kelasBus as $kelas)
                                <option value="{{$kelas->id}}" data-harga="{{$kelas->harga}}">{{$kelas->kelas}}</option>
                                @endforeach
                            </select>                        
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jadwal Keberangkatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="jadwal_keberangkatan" value="{{old('jadwal_keberangkatan')}}" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row" style="align-items:center">
                            <label class="col-sm-2 col-form-label">Jumlah Penumpang<br><small> < 60thn </small></label>
                            <div class="col-sm-10">
                            <input type="number" min="0" name="jumlah_penumpang" value="{{old('jumlah_penumpang')}}" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row" style="align-items:center">
                        <label class="col-sm-2 col-form-label">Jumlah Penumpang<br><small> > 60thn </small></label>
                            <div class="col-sm-10">
                            <input type="number" min="0" name="jumlah_penumpang_lansia" value="{{old('jumlah_penumpang_lansia')}}" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Harga Tiket</label>
                            <div class="col-sm-10">
                            <input type="text" name="harga_tiket" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Total Bayar</label>
                            <div class="col-sm-10">
                            <input type="text" name="total_bayar" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="custom-control custom-radio">
                                <input class="custom-control-input" type="radio" id="customRadio1" name="tanda_persetujuan">
                                <label for="customRadio1" class="custom-control-label">Saya dan/atau rombongan telah membaca, memahami, dan setuju berdasarkan syarat dan ketentuan yang telah ditetapkan</label>
                            </div>                        
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Pesan Tiket</button>
                        <a href="{{route('order.index')}}" class="btn btn-danger">Batal</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- </main> -->

    <footer class="footer mt-auto fixed-bottom bg-light text-center h-10">
        <strong>Copyright &copy; {{date('Y')}} <a href="#">zzz corp</a>.</strong> All rights reserved.
    </footer>
<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    var harga;
    $("input[name=jadwal_keberangkatan]").datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true
    });
    $("input[name=no_hp],input[name=nik]").on('input', function(){
        $(this).val($(this).val().replace(/\D/g, ''));
    });
    $("#kelas").change(function(){
        harga = $("#kelas option:selected" ).data('harga');        
        $("input[name=harga_tiket]").val('Rp '+harga);
    });

    $("input[name=jumlah_penumpang],input[name=jumlah_penumpang_lansia]").on('input',function(){
        let normal = $("input[name=jumlah_penumpang]").val();
        let lansia = $("input[name=jumlah_penumpang_lansia]").val();
        let potongan = harga-(harga/10);
        let bayar = (harga*normal) + (lansia*potongan);
        $("input[name=total_bayar]").val('Rp '+bayar);
    });
</script>
<!-- Custom JS -->
@stack('js')
</body>
</html>
