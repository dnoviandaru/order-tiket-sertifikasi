@extends('base')
@section('title','Bus')
@section('bus','active bg-warning')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Bus</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('bus.index')}}">Bus</a></li>
                        <li class="breadcrumb-item active">Tambah</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    @foreach($errors->all() as $message)
                    <p class="m-0">{{"$message"}}</p>
                    @endforeach
                </div>
            @endif
            <div class="card card-primary">
                <!-- form start -->
                <form action="{{route('bus.store')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Kelas Bus<span class="text-danger">*</span></label>
                            <input type="text" name="kelas_bus" value="{{old('kelas_bus')}}" class="form-control" placeholder="...">
                        </div>
                        <div class="form-group">
                            <label>Harga<span class="text-danger">*</span></label>
                            <input type="text" name="harga" value="{{old('harga')}}" class="form-control" placeholder="...">
                        </div>
                        <div class="form-group">
                            <label>Gambar<span class="text-danger">*</span><button type="button" class="btn btn-info" id="tbh-gambar">+</button></label>
                            <input type="file" name="gambar[]" class="form-control" placeholder="...">
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('js')
<!-- @include('component/validation') -->
@include('component/select2')
<script>
    $("#tbh-gambar").click(function(){
        let gambar = "<input type='file' name='gambar[]' class='form-control' placeholder='...'>"
        $("#gambar").append(gambar);
    });
</script>
@endpush
