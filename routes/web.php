<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Auth::routes([
    'register' => false, // remove this class to activate register route
    'reset' => false, // remove this class to activate password reset ro+ute
    'confirm' => false, // remove this class to activate password confirmation route
    'verify' => false // remove this class to activate email verification route
]);
Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
    Route::get('gambar/{id}', [App\Http\Controllers\KelasBusController::class, 'listGambar'])->name('bus.gambar');
    Route::resource('component', App\Http\Controllers\ComponentController::class)->except('destroy');
    Route::resource('bus', App\Http\Controllers\KelasBusController::class)->except('destroy');
    Route::delete('component/delete', [App\Http\Controllers\ComponentController::class, 'destroy'])->name('component.destroy');
    Route::delete('bus/delete', [App\Http\Controllers\KelasBusController::class, 'destroy'])->name('bus.destroy');
});
Route::resource('order', App\Http\Controllers\OrderTiketController::class)->except('destroy');
