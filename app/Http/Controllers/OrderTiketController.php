<?php

namespace App\Http\Controllers;

use App\Models\OrderTiket;
use App\Models\KelasBus;
use Illuminate\Http\Request;

class OrderTiketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelasBus = KelasBus::all();
        return view('order.index',compact('kelasBus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'nik'=> 'numeric|digits:16',
            'no_hp'=> 'required|numeric|digits_between:9,14|starts_with:08',
            'kelas_penumpang' => 'required',
            'jadwal_keberangkatan' => 'required',
            'jumlah_penumpang' => 'required',
            'jumlah_penumpang_lansia' => 'required',
            'tanda_persetujuan' => 'required'
        ]);

        $data = new OrderTiket;
        $data->kelas = $request->kelas_penumpang;
        $data->nama = $request->nama;
        $data->nik = $request->nik;
        $data->no_hp = $request->no_hp;
        $data->tgl_berangkat = $request->jadwal_keberangkatan;
        $data->penumpang = $request->jumlah_penumpang;
        $data->penumpang_lansia = $request->jumlah_penumpang_lansia;
        $data->save();
        return redirect()->route('order.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderTiket  $orderTiket
     * @return \Illuminate\Http\Response
     */
    public function show(OrderTiket $orderTiket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderTiket  $orderTiket
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderTiket $orderTiket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderTiket  $orderTiket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderTiket $orderTiket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderTiket  $orderTiket
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderTiket $orderTiket)
    {
        //
    }
}
