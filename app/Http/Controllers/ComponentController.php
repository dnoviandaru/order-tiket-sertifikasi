<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Component;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class ComponentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Component::select('com_cd','code_nm','code_group','code_value')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<div class="btn-group btn-group-sm">';
                    $btn .= '<button type="button" class="btn btn-danger btn-delete" data-toggle="modal" data-target="#modal-delete" data-id="'.$row->com_cd.'"><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></button>';
                    $btn .= '<a href="'.route('component.edit',$row->com_cd).'" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a>';
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $datatable['route'] = route('component.index');
        $datatable['column'] = array('com_cd','code_nm','code_group','code_value');
        return view('komponen.index', compact('datatable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $komponen = Component::distinct()->get('code_group');
        return view('komponen.create',compact('komponen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_komponen' => 'required|unique:components,com_cd',
            'nama_komponen' => 'required',
            'grup_komponen' => 'required',
        ]);

        $data = Component::create([
            'com_cd'      => $request->kode_komponen,
            'code_nm'     => $request->nama_komponen,
            'code_group'  => $request->grup_komponen,
            'code_value'  => $request->value_komponen,
            // 'created_by'  => Auth::id()
        ]);

        if($data->save()){
            return redirect()->route('component.index')->with('message',array('response'=>'Berhasil menambah data','color'=>'success'));
        }else{
            return back()->withInput()->withErrors(['Gagal menambah data']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Component::find($id);
        $komponen = Component::distinct()->get('code_group');
        return view('komponen.edit',compact('data','komponen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_komponen' => 'required',
            'grup_komponen' => 'required',
        ]);

        $data = Component::find($id);
        $data->code_nm    = $request->nama_komponen;
        $data->code_group   = $request->grup_komponen;
        $data->code_value  = $request->value_komponen;
        // $data->updated_by = Auth::id();

        if($data->save()){
            return redirect()->route('component.index')->with('message',array('response'=>'Berhasil mengubah data','color'=>'success'));
        }else{
            return back()->withInput()->withErrors(['Gagal mengubah data']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            Component::findOrFail($request->id)->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('message',array('response'=>'Tidak berhasil menghapus data','color'=>'danger'));
        }
        return redirect()->route('component.index')->with('message',array('response'=>'Berhasil menghapus data','color'=>'success'));
    }
}
