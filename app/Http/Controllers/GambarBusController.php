<?php

namespace App\Http\Controllers;

use App\Models\GambarBus;
use Illuminate\Http\Request;

class GambarBusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GambarBus  $gambarBus
     * @return \Illuminate\Http\Response
     */
    public function show(GambarBus $gambarBus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GambarBus  $gambarBus
     * @return \Illuminate\Http\Response
     */
    public function edit(GambarBus $gambarBus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GambarBus  $gambarBus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GambarBus $gambarBus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GambarBus  $gambarBus
     * @return \Illuminate\Http\Response
     */
    public function destroy(GambarBus $gambarBus)
    {
        //
    }
}
