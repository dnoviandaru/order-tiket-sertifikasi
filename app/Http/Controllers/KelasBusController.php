<?php

namespace App\Http\Controllers;

use App\Models\KelasBus;
use App\Models\GambarBus;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class KelasBusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = KelasBus::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<div class="btn-group btn-group-sm">';
                    $btn .= '<button type="button" class="btn btn-danger btn-delete" data-toggle="modal" data-target="#modal-delete" data-id="'.$row->id.'"><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Hapus"></i></button>';
                    $btn .= '<a href="'.route('bus.edit',$row->id).'" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a>';
                    $btn .= '</div>';
                    return $btn;
                })
                ->addColumn('gambar', function($row){
                    return '<a href="#" data-id="'.$row->id.'" class="lihat-gambar">Lihat gambar</a>';
                })
                ->rawColumns(['action','gambar'])
                ->make(true);
        }
        $datatable['route'] = route('bus.index');
        $datatable['column'] = array('kelas','harga','gambar');
        return view('bus.index', compact('datatable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kelas_bus' => 'required',
            'harga' => 'required|integer',
        ]);

        $data = new KelasBus;
        $data->kelas = $request->kelas_bus;
        $data->harga = $request->harga;
        $data->save();
        if (!empty($request->file('gambar'))) {
            foreach ($request->file('gambar') as $value) {
                $gambarBus = new GambarBus;
                $fileName = \Str::uuid().'.'.$value->getClientOriginalExtension();
                $path = $value->storeAs('public/gambar-bus', $fileName);
                $gambarBus->kelas_bus_id = $data->id;
                $gambarBus->gambar_bus = $fileName;
                $gambarBus->save();
            }
        }
        if($data){
            return redirect()->route('bus.index')->with('message',array('response'=>'Berhasil menambah data','color'=>'success'));
        }else{
            return back()->withInput()->withErrors(['Gagal menambah data']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KelasBus  $kelasBus
     * @return \Illuminate\Http\Response
     */
    public function show(KelasBus $kelasBus)
    {
        //
    }

    public function listGambar($id)
    {
        $data = GambarBus::where('kelas_bus_id',$id)->get();
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KelasBus  $kelasBus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = KelasBus::find($id);
        return view('bus.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KelasBus  $kelasBus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'kelas_bus' => 'required',
            'harga' => 'required|integer',
            'gambar.*' => 'required'
        ]);

        $data = KelasBus::find($id);
        $data->kelas = $request->kelas_bus;
        $data->harga = $request->harga;
        $data->save();
        if (!empty($request->file('gambar'))) {
            foreach ($request->file('gambar') as $value) {
                $gambarBus = new GambarBus;
                $fileName = \Str::uuid().'.'.$value->getClientOriginalExtension();
                $path = $value->storeAs('public/gambar-bus', $fileName);
                $gambarBus->kelas_bus_id = $data->id;
                $gambarBus->gambar_bus = $fileName;
                $gambarBus->save();
            }
        }

        if($data){
            return redirect()->route('bus.index')->with('message',array('response'=>'Berhasil mengubah data','color'=>'success'));
        }else{
            return back()->withInput()->withErrors(['Gagal menambah data']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KelasBus  $kelasBus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            KelasBus::findOrFail($request->id)->delete();
        } catch (\Throwable $th) {
            // dd($th);
            return redirect()->back()->with('message',array('response'=>'Tidak berhasil menghapus data','color'=>'danger'));
        }
        return redirect()->route('bus.index')->with('message',array('response'=>'Berhasil menghapus data','color'=>'success'));
    }
}
