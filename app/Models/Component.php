<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    use HasFactory;
    protected $primaryKey   = 'com_cd';
    public $incrementing    = false;
    protected $keyType = 'string';

    protected $fillable = [
        'com_cd', 'code_nm', 'code_group','code_value','created_by', 'update_by'
    ];

    public static function getComCd($codeGroup){
        $code = Component::where('code_group', $codeGroup)->count();

        return $codeGroup.'_'.str_pad($code + 1 , 2 , "0" ,STR_PAD_LEFT);
    }

    public function childrens()
    {
        return $this->hasMany('App\Models\Component', 'code_group', 'com_cd')->with('childrens')->orderBy(\DB::raw('LENGTH(com_cd), com_cd'), 'asc');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Component', 'code_group', 'com_cd');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Component', 'code_group', 'com_cd');
    }

    public function sibling()
    {
        return $this->hasMany('App\Models\Component', 'code_group', 'code_group')->orderBy(\DB::raw('LENGTH(com_cd), com_cd'), 'asc');
    }
}
